$(document).ready(function() {
	$('.losb-menu-element').on('click', function() {
		$('.losb-menu-element').removeClass('active');
		$(this).addClass('active');

		var id = $(this).attr('data-id');
		if(id) {
			switchLosb(id);
		}
	});

	$('.losb-menu-element-sub').on('click', function(){
		$('.losb-menu-element-sub').removeClass('active');
		$(this).addClass('active');
		
		var id = $(this).attr('data-id');
		if(id) {
			switchLosbSub(id);
		}
	});

	function switchLosb(id) {
		$('.losb-content').removeClass('active');
		$('.losb-content[data-id="losb-content-'+id+'"]').addClass('active');
	}

	function switchLosbSub(id) {
		$('.losb-content-sub').removeClass('active');		
		$('.losb-content-sub[data-id="losb-content-sub-'+id+'"]').addClass('active');
	}
});